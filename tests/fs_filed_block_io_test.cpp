#include <fs_filed_block_io.h>
#include <fstream>
#include <iostream>
#include <cstring>

int main()
{
    try
    {
        std::fstream ofile("/tmp/disk.img", std::ios::out | std::ios::in);
        ofile << "Hello, world!" << std::endl;
        ofile.close();

        char buff[1024]{};
        fs_filed_block_io filedBlockIo;
        filedBlockIo.open("/tmp/disk.img");
        filedBlockIo.read(buff, 4, 0, 0);
        filedBlockIo.write("HHHH", 4, 1, 0);
        filedBlockIo.read(buff, 4, 2, 0);
        filedBlockIo.close();

        if (!strcmp(buff, "HHH,"))
        {
            return 0;
        }

        return 1;
    }
    catch (std::exception & err)
    {
        std::cerr << err.what() << std::endl;
    }
}
