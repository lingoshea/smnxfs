#include <data_blk_io.h>
#include <fstream>
#include <iostream>
#include <cstring>

int main()
{
    try
    {
        std::fstream ofile("/tmp/disk.img", std::ios::out | std::ios::in);
        ofile.close();

        fs_filed_block_io filedBlockIo;
        filedBlockIo.open("/tmp/disk.img");

        smnxfs_super_block_t superBlock =
                {
                        .block_count = 6,
                        .bitmap_block_count = 1,
                        .data_block_count = 4
                };
        filedBlockIo.write((char*)&superBlock, sizeof(superBlock), 0, 0);
        smnxfs_block_head_t head { .data_checksum = 34 };
        filedBlockIo.write((char*)&head, sizeof(head), 0, 2);

        char buff[1024] { };
        const char * data = "Hello, world!";
        data_blk_io data_block(2, filedBlockIo);
//        filedBlockIo.write(data, strlen(data), 0, 2);
        data_block.write(data, strlen(data), 0, 0);
        data_block.read(buff, 4, 1, 0);
//        filedBlockIo.read(buff, 4, 1, 2);

        if (!strcmp("ello", buff))
        {
            return 0;
        }

        return 1;
    }
    catch (std::exception & err)
    {
        std::cerr << err.what() << std::endl;
    }
}
