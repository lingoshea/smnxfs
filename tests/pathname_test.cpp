#include <pathname.h>
#include <fstream>
#include <iostream>
#include <cstring>

int main()
{
    try
    {
        smnxfs_pathname_t pathname("tmp/tmp/tmp");
        for (auto & i : pathname.get_direct_pathname())
        {
            if (i != "tmp")
            {
                return 1;
            }
        }

        return 0;
    }
    catch (std::exception & err)
    {
        std::cerr << err.what() << std::endl;
    }
}
