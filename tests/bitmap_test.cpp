#include <bitmap.h>
#include <fs_filed_block_io.h>
#include <fstream>
#include <iostream>
#include <smnxfs_error.h>

int main()
{
    try
    {
        std::fstream ofile("/tmp/disk.img", std::ios::out | std::ios::in);
        ofile.close();

        fs_filed_block_io filedBlockIo;
        filedBlockIo.open("/tmp/disk.img");

        smnxfs_super_block_t superBlock =
                {
                .block_count = 6,
                .bitmap_block_count = 1,
                .data_block_count = 4
                };
        filedBlockIo.write((char*)&superBlock, sizeof(superBlock), 0, 0);

        bitmap my_bitmap(filedBlockIo);
        my_bitmap.set_bit_of(0, true);
        my_bitmap.set_bit_of(1, true);
        my_bitmap.set_bit_of(2, true);
        my_bitmap.set_bit_of(3, true);

        for (uint64_t i = 0; i  < 4; i++)
        {
            if (!my_bitmap.get_bit_of(i))
            {
                return 1;
            }
        }

        my_bitmap.set_bit_of(0, false);
        my_bitmap.set_bit_of(1, false);
        my_bitmap.set_bit_of(2, false);
        my_bitmap.set_bit_of(3, false);

        for (uint64_t i = 0; i  < 4; i++)
        {
            if (my_bitmap.get_bit_of(i))
            {
                return 1;
            }
        }

        my_bitmap.set_bit_of(0, true);
        if (!my_bitmap.get_bit_of(0))
        {
            return 1;
        }
        my_bitmap.set_bit_of(1, false);
        if (my_bitmap.get_bit_of(1))
        {
            return 1;
        }
        my_bitmap.set_bit_of(2, true);
        if (!my_bitmap.get_bit_of(2))
        {
            return 1;
        }
        my_bitmap.set_bit_of(3, false);
        if (my_bitmap.get_bit_of(3))
        {
            return 1;
        }

        my_bitmap.set_bit_of(0, false);
        if (my_bitmap.get_bit_of(0))
        {
            return 1;
        }
        my_bitmap.set_bit_of(1, true);
        if (!my_bitmap.get_bit_of(1))
        {
            return 1;
        }
        my_bitmap.set_bit_of(2, false);
        if (my_bitmap.get_bit_of(2))
        {
            return 1;
        }
        my_bitmap.set_bit_of(3, true);
        if (!my_bitmap.get_bit_of(3))
        {
            return 1;
        }

        my_bitmap.set_bit_of(0, true);
        if (!my_bitmap.get_bit_of(0))
        {
            return 1;
        }
        if (!my_bitmap.get_bit_of(0))
        {
            return 1;
        }
        my_bitmap.set_bit_of(1, true);
        if (!my_bitmap.get_bit_of(1))
        {
            return 1;
        }
        if (!my_bitmap.get_bit_of(1))
        {
            return 1;
        }
        my_bitmap.set_bit_of(2, false);
        if (my_bitmap.get_bit_of(2))
        {
            return 1;
        }
        if (my_bitmap.get_bit_of(2))
        {
            return 1;
        }
        my_bitmap.set_bit_of(3, false);
        if (my_bitmap.get_bit_of(3))
        {
            return 1;
        }
        if (my_bitmap.get_bit_of(3))
        {
            return 1;
        }

        my_bitmap.set_bit_of(0, false);
        if (my_bitmap.get_bit_of(0))
        {
            return 1;
        }
        if (my_bitmap.get_bit_of(0))
        {
            return 1;
        }
        my_bitmap.set_bit_of(1, true);
        if (!my_bitmap.get_bit_of(1))
        {
            return 1;
        }
        if (!my_bitmap.get_bit_of(1))
        {
            return 1;
        }
        my_bitmap.set_bit_of(2, true);
        if (!my_bitmap.get_bit_of(2))
        {
            return 1;
        }
        if (!my_bitmap.get_bit_of(2))
        {
            return 1;
        }
        my_bitmap.set_bit_of(3, true);
        if (!my_bitmap.get_bit_of(3))
        {
            return 1;
        }
        if (!my_bitmap.get_bit_of(3))
        {
            return 1;
        }

        if (my_bitmap.alloc() != 0)
        {
            return 1;
        }

        my_bitmap.del(3);
        if (my_bitmap.alloc() != 3)
        {
            return 1;
        }

        try
        {
            my_bitmap.set_bit_of(0, true);
            my_bitmap.set_bit_of(1, true);
            my_bitmap.set_bit_of(2, true);
            my_bitmap.set_bit_of(3, true);
            my_bitmap.alloc();

            return 1;
        }
        catch (smnxfs_error_t & err)
        {
            if (err.my_errcode() != SMNXFS_DATA_BLOCK_EXHAUSTED)
            {
                return 1;
            }
        }

        my_bitmap.del(2);
        if (my_bitmap.alloc() != 2)
        {
            return 1;
        }
    }
    catch (std::exception & err)
    {
        std::cerr << err.what() << std::endl;
    }
}
