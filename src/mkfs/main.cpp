#include <iostream>
#include <fs_filed_block_io.h>
#include <cctype>
#include <getopt.h>
#include <smnxfs_error.h>
#include <data_blk_io.h>
#include <inode_io.h>

/// Show help message
/** @param progname Program name, given by *argv **/
void help(const char * progname)
{
    printf("%s [OPTION] [DEVICE]\n"
           "\t--help      Show this help message\n",
           progname);
}

/// make root
void make_root(smnxfs_size_t root_filed_block_id, fs_filed_block_io & filed_block_io)
{
    smnxfs_inode_t root_inode { };
    bitmap fs_bitmap(filed_block_io);
    data_blk_io data_block(root_filed_block_id, filed_block_io);
    inode_io root_inode_io(data_block);

    // set time
    auto cur_time = smnxfs_current_time();
    root_inode.f_stat.st_atim = cur_time;
    root_inode.f_stat.st_ctim = cur_time;
    root_inode.f_stat.st_mtim = cur_time;

    // other info
    root_inode.f_stat.st_nlink = 2;
    root_inode.f_stat.st_mode = S_IFDIR | 0755;

    // write root inode
    data_block.write((char*)&root_inode, sizeof(root_inode), 0, 0);

    // 2 root dentries
    smnxfs_dentry_t dentry_f0 { }, dentry_f1 { };
    strcpy(dentry_f0.name, ".");
    dentry_f0.inode_block = fs_bitmap.alloc();
    strcpy(dentry_f1.name, "..");
    dentry_f1.inode_block = fs_bitmap.alloc();

    // write info
    root_inode_io.write((char*)&dentry_f0, sizeof(dentry_f0), 0, root_filed_block_id);
    root_inode_io.write((char*)&dentry_f1, sizeof(dentry_f1), sizeof(dentry_f0), root_filed_block_id);
}

/// Format device
 /**
  * @param pathname device
  * @return true if succeeded, false if failed **/
bool format_device(const char * pathname)
{
    try
    {
        fs_filed_block_io filed_block_io;
        filed_block_io.open(pathname);
        auto block_count = filed_block_io.count_filed_block();
        smnxfs_super_block_t super_block = {
                .magic = SMNXFS_MAGIC_NUMBER,
                .block_count = block_count,
                .data_block_count = static_cast<smnxfs_size_t>(((double) 8 / (double) 9)
                                                               * static_cast<double>(block_count) - 1),
                .last_mount_time = smnxfs_current_time(),
                .magic_comp = ~SMNXFS_MAGIC_NUMBER
        };
        super_block.bitmap_block_count = block_count - super_block.data_block_count - 1;

        char empty_dummy[SMNXFS_BLOCK_LENGTH]{};

        // write super block
        filed_block_io.write(empty_dummy, SMNXFS_BLOCK_LENGTH, 0, 0); // clear super block
        filed_block_io.write((char *) &filed_block_io, sizeof(filed_block_io), 0, 0);

        // clear bitmap
        for (uint64_t i = 1; i <= super_block.bitmap_block_count; i++) {
            filed_block_io.write(empty_dummy, SMNXFS_BLOCK_LENGTH, 0, i); // clear bitmap
        }

        make_root(super_block.bitmap_block_count + 1, filed_block_io);

        // mark root block as used
        char root_bitmap = 0x01;
        filed_block_io.write(&root_bitmap, 1, 0, 1);

        filed_block_io.close();

    }
    catch (smnxfs_error_t & err)
    {
        std::cerr << "Error: " << err.what() << " (errno=" << err.what_errno() << ")" << std::endl;
        return false;
    }
    return true;
}

int main(int argc, char ** argv)
{
    int option;

    while (true)
    {
        int option_index = 0;
        static struct option long_options[] = {
                {"blksize",         required_argument,  nullptr,    0 },
                {"help",            no_argument,        nullptr,    0 },
                {nullptr,           0,                  nullptr,    0 },
        };

        option = getopt_long(argc, argv, "h", long_options, &option_index);

        if (option == -1)
        {
            break;
        }

        switch (option) {
            case 0:
                if (option_index == 1)
                {
                    help(*argv);
                    return 0;
                }

                return 1;

            case 'h':
                help(*argv);
                return 0;

            case '?':
                if (optopt == 'b')
                {
                    fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                }
                else if (isprint (optopt))
                {
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                }

                return 1;

            default:
                return 1;
        }
    }

    if (optind == argc)
    {
        std::cerr << "No device specified!" << std::endl;
        return 1;
    }

    int ret = 0;

    while (optind < argc)
    {
        try
        {
            if (!format_device(argv[optind]))
            {
                std::cerr << "Formatting device `" << argv[optind] << "' FAILED!" << std::endl;
                ret = 1;
            }
            else
            {
                std::cout << "Device `" << argv[optind] << "' formatted!" << std::endl;
            }
        }
        catch (smnxfs_error_t & err)
        {
            std::cerr << "ERROR: " << err.what() << " (errno=" << err.what_errno() << ")" << std::endl;
        }

        optind++;
    }

    return ret;
}
