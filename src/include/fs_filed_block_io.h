#ifndef SMNXFS_FS_FILED_BLOCK_IO_H
#define SMNXFS_FS_FILED_BLOCK_IO_H

#include <smnxfs.h>

typedef int file_t;

// Filed block starts count from 0

/// I/O in filed block
class fs_filed_block_io
{
private:
    /// File descriptor
    file_t device = 0;

public:

    /// Open a device
    /** @param pathname path to device **/
    void open(const char * pathname);

    /// close a device and set file_t device to 0.
    /// This function has no effect if device is already equal to 0
    void close();

    /// read in a specific block, return 0 if device is 0
    /** @param buffer data buffer
     *  @param length buffer length
     *  @param offset read offset
     *  @param block filed block number **/
    smnxfs_size_t read(char * buffer, smnxfs_size_t length, smnxfs_size_t offset, smnxfs_size_t block) const;

    /// write in a specific block, return 0 if device is 0
    /** @param buffer data buffer
     *  @param length buffer length
     *  @param offset write offset
     *  @param block filed block number **/
    smnxfs_size_t write(const char * buffer, smnxfs_size_t length, smnxfs_size_t offset, smnxfs_size_t block) const;

    /// using lseek to count block, return 0 if device is 0
    [[nodiscard]] smnxfs_size_t count_filed_block() const;

    fs_filed_block_io() : device(0) { }
    fs_filed_block_io & operator=(const fs_filed_block_io&&) = delete;
    fs_filed_block_io(const fs_filed_block_io&&) = delete;
};


#endif //SMNXFS_FS_FILED_BLOCK_IO_H
