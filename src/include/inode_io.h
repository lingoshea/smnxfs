#ifndef SMNXFS_INODE_IO_H
#define SMNXFS_INODE_IO_H

#include <data_blk_io.h>

class inode_io
{
    data_blk_io & data_block;
public:
    /// init inode io
    /** @param _dataBlkIo data block I/O manager **/
    explicit inode_io(data_blk_io & _dataBlkIo) : data_block(_dataBlkIo) { }

    /// read data with the info from inode block
    /** @param buffer output buffer
     *  @param length data length
     *  @param offset read offset
     *  @param inode_block inode block data block number **/
    smnxfs_size_t read(char * buffer, smnxfs_size_t length, smnxfs_size_t offset, smnxfs_size_t inode_block);

    /// write data with the info from inode block, updates size info in inode block
    /// (doesn't accept offset beyond file)
    /** @param buffer input buffer
     *  @param length data length
     *  @param offset write offset
     *  @param inode_block inode block data block number **/
    smnxfs_size_t write(const char * buffer, smnxfs_size_t length, smnxfs_size_t offset, smnxfs_size_t inode_block);

};


#endif //SMNXFS_INODE_IO_H
