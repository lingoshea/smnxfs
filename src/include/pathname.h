#ifndef SMNXFS_PATHNAME_H
#define SMNXFS_PATHNAME_H


#include <string>
#include <vector>

typedef std::vector<std::string> pathname_t;

/// SMNXFS Entry
class smnxfs_pathname_t
{
private:
    pathname_t pathname;

public:
    /// create an entry link
    /// @param pathname entry
    explicit smnxfs_pathname_t(std::string pathname);

    [[nodiscard]] pathname_t get_pathname() const { return pathname; }
    [[nodiscard]] pathname_t & get_direct_pathname() { return pathname; }
};


#endif //SMNXFS_PATHNAME_H
