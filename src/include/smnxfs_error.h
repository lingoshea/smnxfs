#ifndef SMNXFS_SMNXFS_ERROR_H
#define SMNXFS_SMNXFS_ERROR_H

#include <string>
#include <utility>
#include <cstring>

#define SMNXFS_CANNOT_OPEN_DEVICE            0x9001  /* Cannot open device with given pathname */
#define SMNXFS_LSEEK_FAILED                  0x9002  /* lseek failed */
#define SMNXFS_FSTAT_FAILED                  0x9003  /* fstat failed */
#define SMNXFS_FREAD_FAILED                  0x9004  /* file read failed */
#define SMNXFS_WRITE_FAILED                  0x9005  /* file write failed */

#define SMNXFS_FILESYSTEM_NOT_RECOGNIZED     0xA001  /* Filesystem not recognized */

#define SMNXFS_BLOCK_LINK_ALREADY_OCCUPIED   0xB001  /* Data block link already occupied */
#define SMNXFS_DATA_BLOCK_EXHAUSTED          0xB002  /* Data block is exhausted */

/// Filesystem Error
class smnxfs_error_t : public std::exception
{
private:
    unsigned int error_code;
    error_t _errno;

public:
    /// Generate a error with error code
    /** @param _code Your error code **/
    explicit smnxfs_error_t(unsigned int _code) noexcept : error_code(_code), _errno(errno) {}

    /// Return explanation of current error
    [[nodiscard]] const char * what() const noexcept override;

    /// Return the explanation of errno snapshoted when the current error is generated
    [[nodiscard]] const char * what_errno() const noexcept { return strerror(_errno); };

    /// Return the errno snashoted when the current error is generated
    [[nodiscard]] error_t my_errno() const noexcept { return _errno; }

    /// Return the error code of current error
    [[nodiscard]] unsigned int my_errcode() const noexcept { return error_code; }
};


#endif //SMNXFS_SMNXFS_ERROR_H
