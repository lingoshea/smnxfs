#ifndef SMNXFS_BITMAP_H
#define SMNXFS_BITMAP_H

#include <fs_filed_block_io.h>

// bitmap starts count from 0

class bitmap
{
private:
    fs_filed_block_io & filed_block_io;

public:
    /// init with filed block io
    /** @param _filedBlockIo Filed Block I/O manager **/
    explicit bitmap(fs_filed_block_io & _filedBlockIo) : filed_block_io(_filedBlockIo) { }

    /// allocate a new empty block
    smnxfs_size_t alloc();

    /// delete a block
    /** @param data_block Data block number to delete **/
    void del(smnxfs_size_t data_block);

    /// get bit with specific offset
    /** @param bit bit offset **/
    bool get_bit_of(smnxfs_size_t bit);

    /// set bit with specific offset
    /** @param bit bit offset
     *  @param val value **/
    void set_bit_of(smnxfs_size_t bit, bool val);
};


#endif //SMNXFS_BITMAP_H
