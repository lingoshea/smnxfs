#ifndef SMNXFS_SMNXFS_H
#define SMNXFS_SMNXFS_H

#define SMNXFS_BLOCK_LENGTH (4 * 1024) /* 4Kb per block */
#define SMNXFS_MAGIC_NUMBER (0x960047FEA3B389FC)

#include <cstdint>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctime>

typedef uint64_t smnxfs_size_t;
typedef uint64_t smnxfs_flag_t;
typedef uint64_t smnxfs_checksum_t;

/// filesystem super block
struct smnxfs_super_block_t
{
    uint64_t      magic;                // magic number
    smnxfs_size_t block_count;          // all block count (include super block)
    smnxfs_size_t bitmap_block_count;   // bitmap block count
    smnxfs_size_t data_block_count;     // data block count (also equals to bitmap size)
    smnxfs_size_t last_alloc_blk;       // last allocated block
    struct timespec last_mount_time;    // last mount time
    uint64_t      magic_comp;           // complement of magic number
};

/// data block head
struct smnxfs_block_head_t
{
    smnxfs_flag_t       if_snapshoted:1;    // if this block has been snapshoted
    smnxfs_checksum_t   data_checksum;      // checksum of data filed (size: 4K - sizeof(smnxfs_block_head_t))
    smnxfs_size_t       next_block;         // next data block id, 0 if none
};

/// inode
struct smnxfs_inode_t
{
    struct stat     f_stat;             // file stat
    smnxfs_size_t   init_data_block;    // initial data block id, 0 if none
};

/// directory entry (256 bit)
struct smnxfs_dentry_t
{
    smnxfs_size_t   inode_block;    // inode block id
    char            name            // entry name
    [256 - sizeof(inode_block)];
};

/// get current time
timespec smnxfs_current_time();

#define SMNXFS_DATA_LENGTH (SMNXFS_BLOCK_LENGTH - sizeof (smnxfs_block_head_t))

#endif //SMNXFS_SMNXFS_H
