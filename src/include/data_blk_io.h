#ifndef SMNXFS_DATA_BLK_IO_H
#define SMNXFS_DATA_BLK_IO_H

#include <fs_filed_block_io.h>
#include <bitmap.h>

// data block starts count from 0

class data_blk_io
{
    smnxfs_size_t data_blk_starting_filed_blk;
    fs_filed_block_io & filed_io;
    bitmap fs_bitmap;

public:
    /// init data block
    /** @param _data_blk_starting_filed_blk data block starting filed block ID == 1 + bitmap_blk_cnt
     *  @param _filed_io filed I/O manager **/
    explicit data_blk_io(smnxfs_size_t _data_blk_starting_filed_blk,
                         fs_filed_block_io & _filed_io) :
                         data_blk_starting_filed_blk(_data_blk_starting_filed_blk),
                         filed_io(_filed_io), fs_bitmap(_filed_io)
                         { }

    /// read in a specific data block
    /** @param buffer data buffer
     *  @param length buffer length
     *  @param offset read offset
     *  @param block data block number
     **/
    smnxfs_size_t read(char * buffer,
                       smnxfs_size_t length,
                       smnxfs_size_t offset,
                       smnxfs_size_t block);

    /// write in a specific data block
    /** @param buffer data buffer
     *  @param length buffer length
     *  @param offset write offset
     *  @param block data block number
     **/
    smnxfs_size_t write(const char * buffer,
                        smnxfs_size_t length,
                        smnxfs_size_t offset,
                        smnxfs_size_t block);

    /// append a new data block to existing data block, mark used in bitmap
    /** @param data_block the block to append to **/
    smnxfs_size_t append_block(smnxfs_size_t data_block);

    data_blk_io & operator=(const data_blk_io&&) = delete;
    data_blk_io(const data_blk_io&&) = delete;
};


#endif //SMNXFS_DATA_BLK_IO_H
