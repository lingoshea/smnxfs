#include <smnxfs_error.h>

const char *smnxfs_error_t::what() const noexcept
{
    switch (error_code)
    {
        case SMNXFS_CANNOT_OPEN_DEVICE:
            return "Cannot open device";

        case SMNXFS_LSEEK_FAILED:
            return "CAnnot seek device";

        case SMNXFS_FSTAT_FAILED:
            return "Cannot fstat device";

        case SMNXFS_FREAD_FAILED:
            return "Read filed";

        case SMNXFS_WRITE_FAILED:
            return "Write failed";

        case SMNXFS_FILESYSTEM_NOT_RECOGNIZED:
            return "Filesystem not recognized";

        case SMNXFS_BLOCK_LINK_ALREADY_OCCUPIED:
            return "Data block link already occupied";

        case SMNXFS_DATA_BLOCK_EXHAUSTED:
            return "Data block exhausted";

        default:
            return "Unknown";
    }
}

