#include <bitmap.h>
#include <smnxfs_error.h>

smnxfs_size_t bitmap::alloc()
{
    // FIXME: low efficiency
    smnxfs_size_t last_alloc_blk;
    smnxfs_super_block_t sb{};

    filed_block_io.read((char*)&sb, sizeof(sb), 0, 0);
    last_alloc_blk = sb.last_alloc_blk + 1;

    if (!get_bit_of(last_alloc_blk))
    {
        set_bit_of(last_alloc_blk, true);
        sb.last_alloc_blk = last_alloc_blk;
        filed_block_io.write((char*)&sb, sizeof(sb), 0, 0);

        return last_alloc_blk;
    }

    for (last_alloc_blk = 0; last_alloc_blk < sb.data_block_count; last_alloc_blk++)
    {
        if (!get_bit_of(last_alloc_blk))
        {
            set_bit_of(last_alloc_blk, true);
            sb.last_alloc_blk = last_alloc_blk;
            filed_block_io.write((char*)&sb, sizeof(sb), 0, 0);

            return last_alloc_blk;
        }
    }

    throw smnxfs_error_t(SMNXFS_DATA_BLOCK_EXHAUSTED);
}

void bitmap::del(smnxfs_size_t data_block)
{
    set_bit_of(data_block, false);
}

bool bitmap::get_bit_of(smnxfs_size_t bit)
{
    uint64_t bit_loc = bit / 8;
    uint64_t bitmap_block = bit / (8 * SMNXFS_BLOCK_LENGTH) + 1;
    char buf, comp = static_cast<char>((0x01) << (bit % 8));

    filed_block_io.read(&buf, 1, bit_loc, bitmap_block);

    return comp & buf;
}

void bitmap::set_bit_of(smnxfs_size_t bit, bool val)
{
    uint64_t bit_loc = bit / 8;
    uint64_t bitmap_block = bit / (8 * SMNXFS_BLOCK_LENGTH) + 1;
    char buf, comp = static_cast<char>((0x01) << (bit % 8));

    filed_block_io.read(&buf, 1, bit_loc, bitmap_block);

    if (val)
    {
        buf |= comp;
    }
    else
    {
        buf &= (~comp);
    }

    filed_block_io.write(&buf, 1, bit_loc, bitmap_block);
}
