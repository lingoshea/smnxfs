#include <data_blk_io.h>
#include <xxhash64.h>
#include <smnxfs_error.h>

smnxfs_size_t data_blk_io::read(char *buffer, smnxfs_size_t length, smnxfs_size_t offset, smnxfs_size_t block)
{
    return filed_io.read(buffer, length,
                         offset + sizeof(smnxfs_block_head_t),
                         block + data_blk_starting_filed_blk);
}

smnxfs_size_t data_blk_io::write(const char *buffer,
                                 smnxfs_size_t length,
                                 smnxfs_size_t offset,
                                 smnxfs_size_t block)
{
    char dummy[SMNXFS_DATA_LENGTH] { };
    smnxfs_block_head_t head{};

    auto ret = filed_io.write(buffer, length, offset + sizeof(smnxfs_block_head_t),
                              block + data_blk_starting_filed_blk);

    // read block head
    filed_io.read((char *) &head, sizeof(smnxfs_block_head_t), 0,
                  block + data_blk_starting_filed_blk);

    // read full data filed
    filed_io.read(dummy, SMNXFS_DATA_LENGTH, sizeof(smnxfs_block_head_t),
                  block + data_blk_starting_filed_blk);

    // calculate block hash
    head.data_checksum = XXHash64::hash(dummy, SMNXFS_DATA_LENGTH, SMNXFS_MAGIC_NUMBER);

    // update info
    filed_io.write((char*)&head, sizeof(smnxfs_block_head_t), 0,
                   block + data_blk_starting_filed_blk);

    return ret;
}

smnxfs_size_t data_blk_io::append_block(smnxfs_size_t data_block)
{
    // read head
    smnxfs_block_head_t head { };
    filed_io.read((char*)&head, sizeof (head), 0, data_block + data_blk_starting_filed_blk);

    // check if empty
    if (head.next_block != 0)
    {
        throw smnxfs_error_t(SMNXFS_BLOCK_LINK_ALREADY_OCCUPIED);
    }

    // alloc
    head.next_block = fs_bitmap.alloc();

    // save info
    filed_io.write((char*)&head, sizeof (head), 0, data_block + data_blk_starting_filed_blk);

    return head.next_block;
}

