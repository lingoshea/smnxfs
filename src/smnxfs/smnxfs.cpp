#include <smnxfs.h>

timespec smnxfs_current_time()
{
    timespec ts{};
    timespec_get(&ts, TIME_UTC);
    return ts;
}
