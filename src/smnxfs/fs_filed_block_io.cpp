#include <fs_filed_block_io.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <smnxfs_error.h>

void fs_filed_block_io::close()
{
    if (device)
    {
        ::close(device);
    }

    device = 0;
}

void fs_filed_block_io::open(const char *pathname)
{
    device = ::open(pathname, O_RDWR);
    if (device == -1)
    {
        device = 0;
        throw smnxfs_error_t(SMNXFS_CANNOT_OPEN_DEVICE);
    }
}

smnxfs_size_t fs_filed_block_io::count_filed_block() const
{
    if (!device)
    {
        return 0;
    }

    struct stat info{};

    if (lseek(device, 0, SEEK_END) == -1)
    {
        throw smnxfs_error_t(SMNXFS_LSEEK_FAILED);
    }

    if (fstat(device, &info) == -1)
    {
        throw smnxfs_error_t(SMNXFS_LSEEK_FAILED);
    }

    return(info.st_size / SMNXFS_BLOCK_LENGTH);
}

smnxfs_size_t fs_filed_block_io::read(char *buffer,
                                      smnxfs_size_t length,
                                      smnxfs_size_t offset,
                                      smnxfs_size_t block) const
{
    if (!device)
    {
        return 0;
    }

    if (lseek(device,
              static_cast<off64_t>(block * SMNXFS_BLOCK_LENGTH + offset),
              SEEK_SET) == -1)
    {
        throw smnxfs_error_t(SMNXFS_LSEEK_FAILED);
    }

    int64_t ret = ::read(device, buffer, length);

    if (ret == -1)
    {
        throw smnxfs_error_t(SMNXFS_FREAD_FAILED);
    }

    return ret;

}

smnxfs_size_t fs_filed_block_io::write(const char *buffer,
                                       smnxfs_size_t length,
                                       smnxfs_size_t offset,
                                       smnxfs_size_t block) const
{
    if (!device)
    {
        return 0;
    }

    if (lseek(device,
              static_cast<off64_t>(block * SMNXFS_BLOCK_LENGTH + offset),
              SEEK_SET) == -1)
    {
        throw smnxfs_error_t(SMNXFS_LSEEK_FAILED);
    }

    int64_t ret = ::write(device, buffer, length);

    if (ret == -1)
    {
        throw smnxfs_error_t(SMNXFS_WRITE_FAILED);
    }

    return ret;
}


