#include <inode_io.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

smnxfs_size_t
inode_io::read(char *buffer, smnxfs_size_t length, smnxfs_size_t offset, smnxfs_size_t inode_block)
{
    smnxfs_inode_t inode { };
    smnxfs_size_t read_offset = 0;
    data_block.read((char*)&inode, sizeof(inode), 0, inode_block);
    smnxfs_size_t current_read_block = inode.init_data_block;

    // empty read
    if (inode.f_stat.st_size <= offset)
    {
        return 0;
    }

    // calculate actual full length
    smnxfs_size_t actual_full_size = MIN(length + offset, inode.f_stat.st_size);

    /////////////////////////////////////////////////////////////////////////////////////
    /// orphaned head
    auto orphaned_head = offset % SMNXFS_DATA_LENGTH;
    auto orphaned_head_skip = offset / SMNXFS_DATA_LENGTH;

    // skip front blocks
    smnxfs_block_head_t block_head{};
    for (uint64_t i = 0; i < orphaned_head_skip; i++)
    {
        data_block.read((char*)&block_head, sizeof(block_head), 0, current_read_block);
        current_read_block = block_head.next_block;
    }

    data_block.read(buffer, SMNXFS_DATA_LENGTH - orphaned_head,
                    orphaned_head, current_read_block);
    read_offset += SMNXFS_DATA_LENGTH - orphaned_head;
    /////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////
    /// full blocks
    auto full_read = (actual_full_size - read_offset) / SMNXFS_DATA_LENGTH;
    for (uint64_t i = 0; i < full_read; i++)
    {
        smnxfs_block_head_t full_block_head{};
        // read block head
        data_block.read((char*)&full_block_head, sizeof(full_block_head), 0, current_read_block);
        current_read_block = full_block_head.next_block;
        data_block.read(buffer + read_offset, SMNXFS_DATA_LENGTH, 0, current_read_block);
        read_offset += SMNXFS_DATA_LENGTH;
    }
    /////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////
    /// orphaned tail
    auto orphaned_tail = actual_full_size % SMNXFS_DATA_LENGTH;
    smnxfs_block_head_t tail_block_head{};
    // read block head
    data_block.read((char*)&tail_block_head, sizeof(tail_block_head), 0, current_read_block);
    current_read_block = tail_block_head.next_block;
    data_block.read(buffer + read_offset, orphaned_tail, 0, current_read_block);
    read_offset += orphaned_tail;
    /////////////////////////////////////////////////////////////////////////////////////

    return read_offset;
}

smnxfs_size_t
inode_io::write(const char *buffer, smnxfs_size_t length, smnxfs_size_t offset, smnxfs_size_t inode_block)
{
    smnxfs_inode_t inode { };
    smnxfs_size_t write_offset = 0;
    data_block.read((char*)&inode, sizeof(inode), 0, inode_block);
    smnxfs_size_t current_write_block = inode.init_data_block;

    // empty write
    if (inode.f_stat.st_size <= offset)
    {
        return 0;
    }

    /////////////////////////////////////////////////////////////////////////////////////
    /// orphaned head
    auto orphaned_head = offset % SMNXFS_DATA_LENGTH;
    auto orphaned_head_skip = offset / SMNXFS_DATA_LENGTH;

    // skip front blocks
    smnxfs_block_head_t block_head{};
    for (uint64_t i = 0; i < orphaned_head_skip; i++)
    {
        data_block.read((char*)&block_head, sizeof(block_head), 0, current_write_block);
        current_write_block = block_head.next_block;
    }

    data_block.write(buffer, SMNXFS_DATA_LENGTH - orphaned_head,
                    orphaned_head, current_write_block);
    write_offset += SMNXFS_DATA_LENGTH - orphaned_head;
    /////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////
    /// full blocks
    auto full_read = (length + offset - write_offset) / SMNXFS_DATA_LENGTH;
    for (uint64_t i = 0; i < full_read; i++)
    {
        smnxfs_block_head_t full_block_head{};

        // read block head
        data_block.read((char *) &full_block_head, sizeof(full_block_head), 0, current_write_block);

        // check if end
        if (full_block_head.next_block == 0)
        {
            full_block_head.next_block = data_block.append_block(current_write_block);
        }

        current_write_block = full_block_head.next_block;

        data_block.write(buffer + write_offset, SMNXFS_DATA_LENGTH, 0, current_write_block);
        write_offset += SMNXFS_DATA_LENGTH;
    }
    /////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////
    /// orphaned tail
    auto orphaned_tail = (length + offset) % SMNXFS_DATA_LENGTH;
    smnxfs_block_head_t tail_block_head{};
    // read block head
    data_block.read((char*)&tail_block_head, sizeof(tail_block_head), 0, current_write_block);

    // check if end
    if (tail_block_head.next_block == 0)
    {
        tail_block_head.next_block = data_block.append_block(current_write_block);
    }

    current_write_block = tail_block_head.next_block;
    data_block.write(buffer + write_offset, orphaned_tail, 0, current_write_block);
    write_offset += orphaned_tail;
    /////////////////////////////////////////////////////////////////////////////////////

    return write_offset;
}
